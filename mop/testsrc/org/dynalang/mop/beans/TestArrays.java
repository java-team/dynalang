/*
   Copyright 2007 Attila Szegedi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.dynalang.mop.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.dynalang.mop.MetaobjectProtocol;
import org.dynalang.mop.BaseMetaobjectProtocol.Result;

/**
 * @author Attila Szegedi
 * @version $Id: $
 */
public class TestArrays extends TestCase {

    private MetaobjectProtocol mop;

    @Override
    protected void setUp() throws Exception {
        mop = new BeansMetaobjectProtocol(true);
    }
    
    public TestArrays(String name) {
	super(name);
    }
    
    public void testLength() {
	int[] x = new int[5];
	assertEquals(Integer.valueOf(5), mop.get(x, "length"));
	assertEquals(Boolean.TRUE, mop.has(x, "length"));
	assertEquals(Result.notWritable, mop.put(x, "length", 1, mop));
    }
    
    public void testGet() {
	int[] x = new int[] {14, 11, 1974 };
	
	assertEquals(Integer.valueOf(14), mop.get(x, Integer.valueOf(0)));
	assertEquals(Integer.valueOf(11), mop.get(x, 1L));
	assertEquals(Integer.valueOf(1974), mop.get(x, Integer.valueOf(2)));
	
	assertEquals(Result.doesNotExist, mop.get(x, -1L));
	assertEquals(Result.doesNotExist, mop.get(x, Integer.valueOf(3)));
    }

    public void testHas() {
	int[] x = new int[] {14, 11, 1974 };
	
	assertEquals(Boolean.TRUE, mop.has(x, Integer.valueOf(0)));
	assertEquals(Boolean.TRUE, mop.has(x, 1));
	assertEquals(Boolean.TRUE, mop.has(x, Integer.valueOf(2)));
	
	assertEquals(Boolean.FALSE, mop.has(x, -1));
	assertEquals(Boolean.FALSE, mop.has(x, Integer.valueOf(3)));
    }
    
    public void testPut() {
	int[] x = new int[2];
	assertEquals(Result.ok, mop.put(x, Integer.valueOf(0), Integer.valueOf(19), mop));
	assertEquals(Result.ok, mop.put(x, Integer.valueOf(1), Integer.valueOf(6), mop));
	assertEquals(Integer.valueOf(19), mop.get(x, Integer.valueOf(0)));
	assertEquals(Integer.valueOf(6), mop.get(x, Integer.valueOf(1)));
	assertEquals(19, x[0]);
	assertEquals(6, x[1]);
	assertEquals(Result.doesNotExist, mop.put(x, Integer.valueOf(2), Integer.valueOf(6), mop));
	assertEquals(Result.noRepresentation, mop.put(x, Integer.valueOf(1), "xx", mop));
    }
    
    public void testPropertyIds() {
	int[] x = new int[2];
	Iterator<? extends Object> it = mop.propertyIds(x);
	assertTrue(it.hasNext());
	assertEquals(Integer.valueOf(0), it.next());
	assertUnremovable(it);
	assertTrue(it.hasNext());
	assertEquals(Integer.valueOf(1), it.next());
	assertTrue(it.hasNext());
	assertEquals("length", it.next());
	assertUnremovable(it);
	List<Object> s = new ArrayList<Object>();
	while(it.hasNext()) {
	    s.add(it.next());
	    assertUnremovable(it);
	}
	assertEquals(Arrays.asList(new Object[] { 
		"class", "equals", "getClass", "hashCode", "notify", "notifyAll", "toString", "wait" }), s);
    }

    public void testProperties() {
	int[] x = new int[] { 1999, 2001 };
	Iterator<Map.Entry> it = mop.properties(x);
	Map.Entry e = it.next();
	assertEquals(Integer.valueOf(0), e.getKey());
	assertEquals(Integer.valueOf(1999), e.getValue());
	e = it.next();
	assertEquals(Integer.valueOf(1), e.getKey());
	assertEquals(Integer.valueOf(2001), e.getValue());
	// array elements can be set
	e.setValue(Integer.valueOf(2005));
	assertEquals(2005, x[1]);
	e = it.next();
	// non-writable properties can not be set
	assertEquals("length", e.getKey());
	try {
	    e.setValue("x");
	}
	catch(UnsupportedOperationException ex) {
	    // expected - can't set length
	}
	e = it.next();
	assertEquals("class", e.getKey());
	try {
	    e.setValue("x");
	}
	catch(UnsupportedOperationException ex) {
	    // expected - can't set read-only property
	}
	e = it.next();
	assertEquals("equals", e.getKey());
	try {
	    e.setValue("x");
	}
	catch(UnsupportedOperationException ex) {
	    // expected - can't set method
	}
    }
	
    private static void assertUnremovable(Iterator it) {
	try {
	    it.remove();
	    fail();
	}
	catch(UnsupportedOperationException e) {
	    // expected
	}
    }
}
