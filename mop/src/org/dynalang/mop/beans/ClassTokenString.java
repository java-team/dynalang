package org.dynalang.mop.beans;

import org.dynalang.classtoken.ClassToken;

/**
 * @author Attila Szegedi
 * @version $Id: $
 */
public class ClassTokenString {
    private final ClassToken[] classTokens;

    ClassTokenString(Class<?>[] classes) {
        classTokens = new ClassToken[classes.length];
        for(int i = 0; i < classes.length; ++i) {
            classTokens[i] = ClassToken.forClass(classes[i]);
        }
    }
    
    public int hashCode() {
        int hash = 0;
        for(int i = 0; i < classTokens.length; ++i) {
            hash ^= classTokens[i].hashCode();
        }
        return hash;
    }
    
    public boolean equals(Object o) {
        if(o instanceof ClassTokenString) {
            ClassTokenString ct = (ClassTokenString)o;
            if(ct.classTokens.length != classTokens.length) {
                return false;
            }
            for(int i = 0; i < classTokens.length; ++i) {
        	// ClassToken objects are canonicalized, so we can use identity 
        	// comparison here.
                if(ct.classTokens[i] != classTokens[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    boolean containsAny(ClassToken[] tokens) {
	for (int i = 0; i < classTokens.length; i++) {
	    ClassToken classToken = classTokens[i];
	    for (int j = 0; j < tokens.length; j++) {
	        if(classToken == tokens[j]) {
		    return true;
                }
	    }
	}
	return false;
    }
}