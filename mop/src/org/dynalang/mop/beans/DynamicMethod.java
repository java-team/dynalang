/*
   Copyright 2007 Attila Szegedi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.dynalang.mop.beans;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

import org.dynalang.mop.CallProtocol;

/**
 * Represents a single dynamic method. A "dynamic" method is considered to be 
 * the union of all methods of the same name on a class. In the simplest case,
 * it corresponds to a single method, but can actually represent any number of
 * methods if the method name is overloaded. Invoking a dynamic method whose
 * name is overloaded will perform overload resolution.
 * @param <T> the type of class members this dynamic method represents. It is
 * either {@link Method} or {@link Constructor}. 
 * @author Attila Szegedi
 * @version $Id: $
 */
public abstract class DynamicMethod<T extends Member>
{
    static final Object[] NULL_ARGS = new Object[0];
    
    /**
     * Invokes the dynamic method. 
     * @param target the target object -- the "this" of the invoked method, or
     * null for static methods.
     * @param callProtocol the call protocol object used to convert arguments 
     * if needed
     * @param args the arguments to the method. Note that you never need to 
     * pack varargs into an array, nor need to be aware of the vararg concept 
     * at all when using this API. If the called Java method ends up being a 
     * vararg method, its varargs will be packed into an appropriate array 
     * automatically.
     * @return the result of the method invocation.
     */
    public abstract Object call(Object target, CallProtocol callProtocol, Object... args);
    
    static Class<?>[] getParameterTypes(Member member) {
        if(member instanceof Method) {
            return ((Method)member).getParameterTypes();
        }
        if(member instanceof Constructor) {
            return ((Constructor<?>)member).getParameterTypes();
        }
        throw new AssertionError();
    }

    static boolean isVarArgs(Member member) {
        if(member instanceof Method) {
            return ((Method)member).isVarArgs();
        }
        else if(member instanceof Constructor) {
            return ((Constructor<?>)member).isVarArgs();
        }
        throw new AssertionError();
    }
    
    static Object invoke(Member member, Object target, Object... args) 
    throws InstantiationException, IllegalAccessException, InvocationTargetException {
        if(member instanceof Method) {
            return ((Method)member).invoke(target, args);
        }
        else if(member instanceof Constructor) {
            return ((Constructor<?>)member).newInstance(args);
        }
        throw new AssertionError();
    }
}
