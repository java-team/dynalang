package org.dynalang.mop;

import org.dynalang.mop.impl.CompositeClassBasedMetaobjectProtocol;

/**
 * An interface that can be optionally implemented by any MOP if it is strictly
 * class-based (that is, the class of an object used as either target or
 * property identifier solely determines whether it has authority over it or 
 * not). The provision for property identifiers is put in place so that MOPs 
 * can implement special "metaproperties" (i.e. "prototype" in JavaScript) by
 * using property IDs of classes private to their implementation.
 * @author Attila Szegedi
 * @version $Id: $
 */
public interface ClassBasedMetaobjectProtocol extends MetaobjectProtocol {
    
    /**
     * Returns whether this metaobject protocol has authority over objects of 
     * the specified class. Note that if a MOP claims that it has authority 
     * over objects of a certain class, it can still return 
     * {@link BaseMetaobjectProtocol.Result#noAuthority} for certain objects.
     * In that case - when used in {@link CompositeClassBasedMetaobjectProtocol} 
     * - the other participating MOPs will also be given the chance to handle 
     * the object after this MOP was given the chance first. 
     * @param clazz the class of the handled object
     * @return true if this metaobject protocol has authority over objects of 
     * the specified class, false otherwise.
     */
    public boolean isAuthoritativeForClass(Class clazz);
}
